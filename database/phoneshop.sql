-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 18, 2014 at 05:47 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `phoneshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_binhluan`
--

CREATE TABLE IF NOT EXISTS `tbl_binhluan` (
  `id_bl` int(11) NOT NULL AUTO_INCREMENT,
  `id_sp` int(11) NOT NULL,
  `ho_ten` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ngay_gio` datetime NOT NULL,
  `noi_dung` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dien_thoai` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_bl`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_binhluan`
--

INSERT INTO `tbl_binhluan` (`id_bl`, `id_sp`, `ho_ten`, `ngay_gio`, `noi_dung`, `dien_thoai`) VALUES
(1, 27, 'Thái', '2014-03-28 09:16:13', 'Hello ngày mới :v', ''),
(6, 29, 'Thái', '2014-04-17 03:39:12', '<scrip>alert("Hello")</scrip>', ''),
(4, 27, 'Test', '2014-03-28 09:19:31', 'great', ''),
(7, 29, 'Test', '2014-04-17 03:40:54', '<br>error XSS</br>', ''),
(8, 13, 'Test', '2014-04-17 03:45:18', '<scrip>alert(String.fromCharCode(104,101))</scrip>', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ct_ddh`
--

CREATE TABLE IF NOT EXISTS `tbl_ct_ddh` (
  `id_ct_hd` int(11) NOT NULL AUTO_INCREMENT,
  `id_hd` int(11) NOT NULL,
  `id_sp` int(11) NOT NULL,
  `so_luong_mua` int(5) NOT NULL,
  `don_gia` int(12) NOT NULL,
  PRIMARY KEY (`id_ct_hd`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=66 ;

--
-- Dumping data for table `tbl_ct_ddh`
--

INSERT INTO `tbl_ct_ddh` (`id_ct_hd`, `id_hd`, `id_sp`, `so_luong_mua`, `don_gia`) VALUES
(42, 26, 7, 1, 6800000),
(65, 37, 29, 2, 6000000),
(64, 37, 7, 1, 6800000),
(43, 26, 8, 1, 6800000),
(44, 26, 14, 1, 6800000),
(45, 26, 19, 1, 6800000),
(54, 31, 11, 1, 6800000),
(55, 31, 14, 1, 6800000),
(56, 32, 18, 1, 6800000),
(57, 32, 26, 1, 6800000),
(58, 33, 14, 1, 6800000),
(59, 34, 29, 1, 6800000),
(60, 35, 7, 1, 6800000),
(61, 35, 28, 1, 6800000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dm_sanpham`
--

CREATE TABLE IF NOT EXISTS `tbl_dm_sanpham` (
  `id_dm` int(11) NOT NULL AUTO_INCREMENT,
  `ten_dm` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_dm`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_dm_sanpham`
--

INSERT INTO `tbl_dm_sanpham` (`id_dm`, `ten_dm`) VALUES
(1, 'iPhone'),
(2, 'Samsung'),
(3, 'Sony Ericson'),
(4, 'LG'),
(5, 'HTC'),
(6, 'Nokia'),
(7, 'Blackberry'),
(8, 'Asus'),
(9, 'Lenovo'),
(10, 'Motorola');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_don_dh`
--

CREATE TABLE IF NOT EXISTS `tbl_don_dh` (
  `id_hd` int(11) NOT NULL AUTO_INCREMENT,
  `id_kh` int(11) NOT NULL,
  `id_tinh_trang` int(11) NOT NULL,
  `ngay_lap` datetime NOT NULL,
  `tong_gia` int(11) NOT NULL,
  `noi_nhan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_nvgh` int(11) DEFAULT NULL,
  `ghi_chu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_hd`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=38 ;

--
-- Dumping data for table `tbl_don_dh`
--

INSERT INTO `tbl_don_dh` (`id_hd`, `id_kh`, `id_tinh_trang`, `ngay_lap`, `tong_gia`, `noi_nhan`, `id_nvgh`, `ghi_chu`) VALUES
(26, 37, 1, '2014-03-26 16:06:31', 27200000, '207 đê Tô Hoàng', 0, 'Nhập ghi chú....'),
(37, 48, 3, '2014-04-14 09:45:34', 18800000, 'hai bà trưng, hà nội', 1, 'Nhập ghi chú....'),
(31, 42, 1, '2014-03-26 18:18:20', 13600000, '207 đê Tô Hoàng', NULL, NULL),
(32, 43, 1, '2014-03-26 18:25:13', 13600000, '26 hoàng mai', 0, 'Nhập ghi chú....'),
(33, 44, 3, '2014-04-06 10:49:13', 6800000, '34 Nguyễn Văn Cừ', NULL, NULL),
(34, 45, 2, '2014-04-07 10:10:16', 6800000, 'ĐHBK Hà Nội', NULL, NULL),
(35, 46, 2, '2014-04-08 17:22:08', 13600000, '207 đê Tô Hoàng', 2, 'Nhập ghi chú....');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_khachhang`
--

CREATE TABLE IF NOT EXISTS `tbl_khachhang` (
  `id_kh` int(11) NOT NULL AUTO_INCREMENT,
  `ten_kh` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `sdt` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_kh`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=49 ;

--
-- Dumping data for table `tbl_khachhang`
--

INSERT INTO `tbl_khachhang` (`id_kh`, `ten_kh`, `sdt`, `mail`) VALUES
(4, 'Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(5, 'Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(6, 'Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(7, 'Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(8, 'Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(9, 'Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(10, 'Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(11, 'Thai', '098787636', 'thai@gmail.com'),
(12, 'Thai', '098787636', 'thai@gmail.com'),
(13, 'Thai', '098787636', 'thai@gmail.com'),
(14, 'Thai', '098787636', 'thai@gmail.com'),
(15, 'Thai', '098787636', 'thai@gmail.com'),
(16, 'Thai', '098787636', 'thai@gmail.com'),
(17, 'Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(18, 'Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(19, 'Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(20, 'Thai', '098787636', 'thai@gmail.com'),
(21, 'Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(22, 'Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(23, 'Thanh', '098787636', 'thanhnd2807@gmail.com'),
(24, 'thanhtest', '0987987679', 'thanhtest@gmail.com'),
(25, 'Thanh Nd', '0987898767', 'thanh@gmail.com'),
(26, 'Thanh Nd', '0987898767', 'thanh@gmail.com'),
(27, 'Thanh Nd', '0987898767', 'thanh@gmail.com'),
(28, 'Thanh', '098787636', 'thai@gmail.com'),
(29, 'Thanh', '0987898767', 'thanhnd2807@gmail.com'),
(30, 'Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(31, 'Thanh', '098787636', 'thanhnd2807@gmail.com'),
(32, 'Thắng', '0987678765', 'thangnd@gmail.com'),
(33, 'Thanh', '098787636', 'thanhnd2807@gmail.com'),
(34, 'Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(35, 'Thai', '098787636', 'thai@gmail.com'),
(36, 'Thai', '098787636', 'thai@gmail.com'),
(37, 'Thanh', '01694590218', 'thai@gmail.com'),
(38, 'Thanh', '0987678765', 'thai@gmail.com'),
(39, 'Thanh Nd', '098787636', 'thai@gmail.com'),
(40, 'Thanh Nd', '01694590218', 'thai@gmail.com'),
(41, 'Thắng', '01694590218', 'thangnd@gmail.com'),
(42, 'Thanh Nd', '01694590218', 'thanhnd2807@gmail.com'),
(43, 'Thai', '01694590218', 'thai@gmail.com'),
(44, 'Kim Răng Sún', '0989887927', 'kimrs@gmail.com'),
(45, 'Nguyễn Thái Bình', '0918449386', 'nguyenthaibinh@gmail.com'),
(46, 'Nguyễn Danh Thanh', '01694590218', 'thanhnd2807@gmail.com'),
(47, 'Nguyễn Danh Thanh', '0987678765', 'kimrs@gmail.com'),
(48, 'Danh Thanh', '0989878767', 'danhthanh@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nguoidung`
--

CREATE TABLE IF NOT EXISTS `tbl_nguoidung` (
  `id_nd` int(11) NOT NULL AUTO_INCREMENT,
  `tai_khoan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mat_khau` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ten` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_nd`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tbl_nguoidung`
--

INSERT INTO `tbl_nguoidung` (`id_nd`, `tai_khoan`, `mat_khau`, `ten`) VALUES
(1, 'admin', 'admin', 'Admin'),
(2, 'danhthanh', 'thanh', 'Thanh');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nv_gh`
--

CREATE TABLE IF NOT EXISTS `tbl_nv_gh` (
  `id_nvgh` int(11) NOT NULL AUTO_INCREMENT,
  `ten_nvgh` varchar(50) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `sdt_1` varchar(11) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `sdt_2` varchar(11) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`id_nvgh`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_nv_gh`
--

INSERT INTO `tbl_nv_gh` (`id_nvgh`, `ten_nvgh`, `sdt_1`, `sdt_2`) VALUES
(1, 'Nguyễn Quang', '01698783789', NULL),
(2, 'Quang Vinh', '0989876856', '0987573983');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sanpham`
--

CREATE TABLE IF NOT EXISTS `tbl_sanpham` (
  `id_sp` int(11) NOT NULL AUTO_INCREMENT,
  `id_dm` int(11) NOT NULL,
  `id_km` int(11) DEFAULT NULL,
  `ten_sp` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `anh_sp` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gia_sp` int(12) NOT NULL,
  `so_luong` int(5) NOT NULL,
  `kich_thuoc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trong_luong` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mau_sac` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `am_thanh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bo_nho` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `he_dieu_hanh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `the_nho` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `camera` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bao_hanh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ket_noi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gia_km` int(11) DEFAULT NULL,
  `batdau_km` date DEFAULT NULL,
  `ketthuc_km` date DEFAULT NULL,
  PRIMARY KEY (`id_sp`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=43 ;

--
-- Dumping data for table `tbl_sanpham`
--

INSERT INTO `tbl_sanpham` (`id_sp`, `id_dm`, `id_km`, `ten_sp`, `anh_sp`, `gia_sp`, `so_luong`, `kich_thuoc`, `trong_luong`, `mau_sac`, `am_thanh`, `bo_nho`, `he_dieu_hanh`, `the_nho`, `camera`, `pin`, `bao_hanh`, `ket_noi`, `gia_km`, `batdau_km`, `ketthuc_km`) VALUES
(1, 1, NULL, 'IPhone 3GS 32G Màu Đen', 'IPhone-3GS-32G-Mau-Den.jpg', 6800000, 8, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(2, 1, NULL, 'iPhone 4 16G Quốc Tế Trắng', 'iPhone-4-16G-Quoc-Te-Trang.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(3, 1, NULL, 'iPhone 5 16GB Quốc Tế Đen', 'iPhone-5-16GB-Quoc-Te-Den.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(4, 1, NULL, 'iPhone 5C 16GB Blue', 'iPhone-5C-16GB-Blue.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(5, 1, NULL, 'iPhone 5S 32GB Quốc tế Trắng', 'iPhone-5S-32GB-Quoc-te-Mau-Trang.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(6, 2, NULL, 'Samsung Galaxy Note N7000', 'Sam-Galaxy-Note-N7000-pink.jpg', 6800000, 8, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(7, 2, NULL, 'Samsung Galaxy Note 2 đen', 'samsung-galaxy-note-2-den.jpg', 6800000, 8, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(8, 2, NULL, 'Samsung Galaxy Note 3', 'samsung-galaxy-note-3.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(9, 2, NULL, 'Samsung Galaxy S2', 'samsung-galaxy-s2.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(10, 2, NULL, 'Samsung Galaxy S3', 'samsung-galaxy-s3.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(11, 2, NULL, 'Samsung Galaxy S4', 'samsung-galaxy-s4-galaxy.jpg', 6800000, 9, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(12, 3, NULL, 'Sony Arc S (LT18i) Trắng', 'Sony-arc-S-LT18i-Trang.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(13, 3, NULL, 'Sony Arc S', 'Sony-Arc-S.jpg', 6800000, 7, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(14, 3, NULL, 'Sony X10', 'sony-x10.jpg', 6800000, 8, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(15, 3, NULL, 'Sony Xperia Z Màu Đen', 'Sony-Xperia-Z-Mau-Den.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(16, 3, NULL, ' Sony Xperia TX (LT29i) Đen', 'Sony-Xperia-TX-LT29i-Den.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(17, 4, NULL, 'LG F160 Optimus LTE 2', 'LG-F160-Optimus-LTE-2.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(18, 4, NULL, 'LG LTE 3 (LG F260)', 'LG-LTE-3-LG F260.jpg', 6800000, 7, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(19, 4, NULL, 'LG Optimus 2X SU660', 'LG-optimus-2x-SU660.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(20, 4, NULL, 'LG Optimus 3D SU760', 'LG-Optimus-3D-SU760.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(21, 4, NULL, 'LG Optimus G', 'LG-Optimus-G.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(22, 4, NULL, 'LG Optimus L7(LG P705)', 'LG-Optimus-L7LG P705.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(23, 5, NULL, 'HTC EVO 3D', 'HTC-EVO-3D.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(24, 5, NULL, 'HTC One Đen 16GB FPT', 'HTC-One-Den-16GB-cong-ty-FPT.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(25, 5, NULL, 'HTC One Trắng 16GB FPT', 'HTC-One-Trang-16GB-cong-ty-FPT.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(26, 5, NULL, 'HTC one x white', 'htc-one-x-white.jpg', 6800000, 8, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(27, 5, NULL, 'HTC Windows Phone 8S', 'HTC-Windows-Phone-8S.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', NULL, NULL, NULL),
(28, 6, NULL, 'Lumia 800 đen', 'lumia-800-den.jpg', 6800000, 9, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', 6000000, '0000-00-00', '2014-04-17'),
(29, 6, NULL, 'Lumia 900 trắng', 'lumia-900-trang.jpg', 6800000, 5, '2422 x23325', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', 6000000, '2014-04-12', '0000-00-00'),
(30, 6, NULL, 'Lumia 920 hồng', 'lumia-920-hong.jpg', 6800000, 10, '0.99 x 13 x 6.85 cm', '435g', 'Đỏ', 'Vibration; MP3, WAV ringtones', 'Bộ nhớ trong 32g, ram 1g', 'Windows', '2g', '8.7 MP', 'Li-Ion 2000mAh', '12 tháng', 'WIFI, 3G', 6000000, '2014-04-10', '2014-05-16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sp_ban`
--

CREATE TABLE IF NOT EXISTS `tbl_sp_ban` (
  `id_sp_ban` int(11) NOT NULL AUTO_INCREMENT,
  `id_sp` int(11) NOT NULL,
  `so_luong_ban` int(11) NOT NULL,
  PRIMARY KEY (`id_sp_ban`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tbl_sp_ban`
--

INSERT INTO `tbl_sp_ban` (`id_sp_ban`, `id_sp`, `so_luong_ban`) VALUES
(1, 3, 3),
(2, 27, 3),
(3, 30, 3),
(4, 10, 1),
(5, 7, 3),
(6, 8, 1),
(7, 14, 3),
(8, 19, 1),
(9, 1, 3),
(10, 6, 2),
(11, 13, 3),
(12, 18, 3),
(13, 11, 1),
(14, 26, 2),
(15, 29, 5),
(16, 28, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tinh_trang`
--

CREATE TABLE IF NOT EXISTS `tbl_tinh_trang` (
  `id_tinh_trang` int(11) NOT NULL AUTO_INCREMENT,
  `tinh_trang` varchar(255) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  PRIMARY KEY (`id_tinh_trang`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_tinh_trang`
--

INSERT INTO `tbl_tinh_trang` (`id_tinh_trang`, `tinh_trang`) VALUES
(1, 'Chưa chuyển'),
(2, 'Đã chuyển'),
(3, 'Đang chuyển');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
