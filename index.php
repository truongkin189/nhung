<?php
	ob_start();
	session_start();
	include_once('cauhinh/ketnoi.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="javascrip/jquery.min.js"></script>
<title>Shop điện thoại di động</title>
<link href="css/index.css" rel="stylesheet" />
<?php 
    if(isset($_GET['page_layout']))
	switch($_GET['page_layout']){
		case 'chitietsp': echo '<link href="css/chitietsp.css" rel="stylesheet" />';
		break;
		case 'giohang': echo '<link href="css/giohang.css" rel="stylesheet"/>';
		break;
        case 'hoanthanh': echo '<link href="css/hoanthanh.css" rel="stylesheet"/>';
        break;
        case 'muahang': echo '<link href="css/giohang.css" rel="stylesheet"/>'; 
                        echo '<link href="css/muahang.css" rel="stylesheet"/>';
        break;
		default:
		echo '<link href="css/noidung.css" rel="stylesheet" />';
	}
    else echo '<link href="css/noidung.css" rel="stylesheet" />';
?>

</head>

<body>
	<div id="wrapper">
    	<!--Header-->
        	<?php include_once('header.php'); ?>
        <!--End Header-->
        
        <!--menu-->
         	<?php include_once('menu.php'); ?>
        <!--End menu-->
        
        <!--Body-->
        	<div id="body">
            	<!--Sidebar-->
                	<div id="sidebar">
                    	<!--Menu-bar-->
                    	<?php include_once('chucnang/danhmucsp.php'); ?>
                        <!--End menu bar-->
                        
                        <!--Link-->
                        <?php include_once('link.php'); ?>
                        <!--End link-->
                        
                        <!--View-->
                        <?php include_once('view.php'); ?>
                        <!--End view-->
                    </div>
                <!--End sidebar-->
                
                <!--Content-->
                	<div id="content">
                	
                    <!--Search-bar-->
                    	<?php include_once('search.php') ?>
                    <!--End search-bar-->
                    
                    <!--Content-main-->
                        <div id="content-main">
                        	<?php
                                if(isset($_GET['page_layout']))
    								switch($_GET['page_layout']){
    									case 'chitietsp': include_once('chucnang/chitietsp.php');
    									break;
    									case 'danhsachsp': include_once('chucnang/danhsachsp.php');
    									break;
    									case 'giohang': include_once('chucnang/giohang.php');
    									break;
                                        case 'muahang': include_once('chucnang/muahang.php');
                                        break;
                                        case 'spmoi': include_once('chucnang/spmoi.php');
                                        break;
                                        case 'spbanchay': include_once('chucnang/spbanchay.php');
                                        break;
                                        case 'searchlist': include_once('chucnang/searchlist.php');
                                        break;
                                        case 'hoanthanh': include_once('chucnang/hoanthanh.html');
                                        break;
    									default: 
    									include_once('chucnang/noidung.php'); 
    								}
                                else include_once('chucnang/noidung.php'); 
							?> 
                            
                        </div>   
                    <!--End content-main-->
                    </div>
                <!--End content-->
                
                    <!--Product brand-->
                	<?php include_once('thuonghieu.php'); ?>
                <!--End product brand-->
            </div>
        <!--End body-->
        
        <!--Footer-->
        	<?php include_once('footer.php'); ?>
        <!--End footer-->
    </div>
</body>
</html>
