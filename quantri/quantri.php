<?php
	ob_start();
	session_start();
	if(isset($_SESSION['tk'])&&isset($_SESSION['mk'])){
		include_once('../cauhinh/ketnoi.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Quản trị</title>
<link href="../css/quantri.css" rel="stylesheet" />
<link rel="stylesheet" href="../javascrip/jquery-ui.css" />
<script src="../javascrip/jquery-1.8.3.js"></script>
<script src="../javascrip/jquery-ui.js"></script>
<?php
		switch($_GET["page_layout"]){
			case 'danhsachsp': 
				echo '<link rel="stylesheet" type="text/css" href="../css/danhsachsp.css" />';
				break;
			case 'themsp':
				echo '<link rel="stylesheet" type="text/css" href="../css/suasp.css" />';
				break;
			case 'suasp':
				echo '<link rel="stylesheet" type="text/css" href="../css/suasp.css" />';
				break;
			case 'nguoidung':
				echo '<link rel="stylesheet" type="text/css" href="../css/danhsachsp.css" />';
				break;
			case 'themnd':
				echo '<link rel="stylesheet" type="text/css" href="../css/suasp.css" />';
				break;
			case 'suand':
				echo '<link rel="stylesheet" type="text/css" href="../css/suasp.css" />';
				break;
			case 'danhmucsp':
				echo '<link rel="stylesheet" type="text/css" href="../css/danhsachsp.css" />';
				break;
			case 'themnvgh':
				echo '<link rel="stylesheet" type="text/css" href="../css/suasp.css" />';
				break;
			case 'themdm':
				echo '<link rel="stylesheet" type="text/css" href="../css/suasp.css" />';
				break;
			case 'suadm':
				echo '<link rel="stylesheet" type="text/css" href="../css/suasp.css" />';
				break;
			case 'ct_dondathang':
				echo '<link rel="stylesheet" type="text/css" href="../css/ct_dondathang.css" />';
				break;
			default:
				echo '<link rel="stylesheet" type="text/css" href="../css/danhsachsp.css" />';	
		}
		
?>
</head>

<body>
	<div id="wrapper">
    	<!--Side-bar-->
        <div id="side-bar">
        	<!--Home-->
            <div id="home">
            	<a href="quantri.php?page_layout=danhsachsp">Trang chủ</a>
            </div>
            <!--End home-->
            
            <!--Categoris-->
            <div class="categoris">
            	<h2>Quản lý danh mục</h2>
                <ul>
                	<li><a href="quantri.php?page_layout=danhsachsp">Sản phẩm</a></li>
                    <li><a href="quantri.php?page_layout=nguoidung">Người dùng</a></li>
                    <li><a href="quantri.php?page_layout=danhmucsp">Danh mục sản phẩm</a></li>
                    <li><a href="quantri.php?page_layout=nvgiaohang">Nhân viên giao hàng</a></li>
                </ul>
            </div>
            <!--End categoris-->
            
            <!--profession-->
            <div class="categoris">
            	<h2>Quản lý nghiệp vụ</h2>
                <ul>
                	<li><a href="quantri.php?page_layout=dondathang">Đơn đặt hàng</a></li>
                    <li><a href="quantri.php?page_layout=binhluan">Phản hồi</a></li>
                </ul>
            </div>
            <!--End profession-->
            
            <!--System-->
            <div class="categoris">
            	<h2>Quản trị hệ thống</h2>
                <ul>
                	<li><a href="#">Đổi mật khẩu</a></li>
                    <li id="boder-bottom"><a href="dangxuat.php">Đăng xuất</a></li>
                </ul>
            </div>
            <!--End system-->
        </div>
        <!--End side-bar-->
        
        <!--Content-->
        <div id="content">
            <!--Header-->
            <div id="header">
                <div id="them-moi"><a href="quantri.php?page_layout=themsp">Thêm mới sản phẩm</a></div>
                <div id="hello"><label>Xin chào </label><span><?php echo $_SESSION['ten']; ?></span></div>
            </div>
            <!--End header-->
            <!--Content main-->
            <div id="content-main">
             <?php
			 switch($_GET['page_layout']){
				case 'danhsachsp':
					include_once('danhsachsp.php');
					break;
				case 'suasp':
					include_once('suasp.php');
					break;
				case 'themsp':
					include_once('themsp.php');
					break;
				case 'binhluan':
					include_once('binhluan.php');
					break;	
				case 'nguoidung':
					include_once('nguoidung.php');
					break;
				case 'themnd':
					include_once('themnd.php');
					break;
				case 'suand':
					include_once('suand.php');
					break;
				
				case 'danhmucsp':
					include_once('danhmucsp.php');
					break;
				case 'themdm':
					include_once('themdm.php');
					break;
				case 'suadm':
					include_once('suadm.php');
					break;
				case 'dondathang':
					include_once('dondathang.php');
					break;
				case 'ct_dondathang':
					include_once('ct_dondathang.php');
					break;
				case 'nvgiaohang':
					include_once('nvgiaohang.php');
					break;
				case 'themnvgh':
					include_once('themnvgh.php');
				case 'suanvgh':
					include_once('suanvgh.php');
					break;
				case 'huyddh':
					include_once('huyddh.php');
					break;
				default: include_once('danhsachsp.php');
				
			}
			
		?>  
                
            </div>
            <!--End content main-->
       </div>
       <!--End content-->
    </div>
</body>
</html>
<?php
	}
	else
	header('location:dangnhap.php');
?>
