<script type="text/javascript">
    function noteFocus(){
        if(document.frmvien.ghichu.value=='Nhập ghi chú....')
            document.frmvien.ghichu.value='';
    }
    function noteBlur(){
        if(document.frmvien.ghichu.value=='')
            document.frmvien.ghichu.value='Nhập ghi chú....';
    }
    function huyDdh(){
        var conf=confirm('Bạn có chắc chắn muốn hủy đơn hàng này không?');
        return conf;
    }
</script>
<?php 
    ob_start();
    $id_hd=$_GET['id'];

    $sql="SELECT * FROM tbl_don_dh 
                    INNER JOIN tbl_khachhang ON tbl_khachhang.id_kh=tbl_don_dh.id_kh
                    INNER JOIN tbl_tinh_trang ON tbl_tinh_trang.id_tinh_trang=tbl_don_dh.id_tinh_trang
                    WHERE tbl_don_dh.id_hd='$id_hd'";
    $query=mysqli_query($dbConnect ,$sql);
    $row=mysqli_fetch_array($query);

    $sqlNv="SELECT * FROM tbl_nv_gh ";
    $queryNv=mysqli_query($dbConnect ,$sqlNv);

    $sqlSp="SELECT * FROM tbl_don_dh 
                    INNER JOIN tbl_ct_ddh ON tbl_don_dh.id_hd=tbl_ct_ddh.id_hd
                    INNER JOIN tbl_sanpham ON tbl_sanpham.id_sp=tbl_ct_ddh.id_sp
                    WHERE tbl_don_dh.id_hd='$id_hd'";
    $querySp=mysqli_query($dbConnect ,$sqlSp);

    $sqlTt="SELECT * FROM tbl_tinh_trang";
    $queryTt=mysqli_query($dbConnect ,$sqlTt);

    if(isset($_POST['save'])){
        $tinh_trang=$_POST['tinh_trang'];
        $giao_hang=$_POST['giao_hang'];
        $ghi_chu=$_POST['ghichu'];
        if($tinh_trang==3&& $giao_hang==''){
            $error="Bạn phải chọn nhân viên giao hàng !";
        }
        else{
            $sqlUpTtDdh="UPDATE tbl_don_dh SET id_tinh_trang='$tinh_trang', id_nvgh='$giao_hang',ghi_chu='$ghi_chu' WHERE id_hd='$id_hd'";
            $queryUpTtDdh=mysqli_query($dbConnect ,$sqlUpTtDdh);
            header('location:');
        }
        //Neu chon da chuyen thi cap nhat lai so luong ban va so luong san pham
        if($tinh_trang==2){
            $querySpb=mysqli_query($dbConnect ,$sqlSp);
            while($rowSpb=mysqli_fetch_array($querySpb)){
                $id_spb=$rowSpb['id_sp'];
                $SPBan[$id_spb]=$rowSpb['so_luong_mua'];
            }
            foreach($SPBan as $id_spb => $soLuong){
                    $soLuongBan=$soLuong;
                    $sqlCheckIdSpb="SELECT * FROM tbl_sp_ban WHERE id_sp='$id_spb'";
                    $sqlQueryCheckId=mysqli_query($dbConnect ,$sqlCheckIdSpb);
                    $num_row=mysqli_num_rows($sqlQueryCheckId);
                    $row=mysqli_fetch_array($sqlQueryCheckId);
                    if($num_row==1){
                        $id_sp_ban=$row['id_sp_ban'];
                        $soLuong+=$row['so_luong_ban'];
                        $sqlUpSpb="UPDATE tbl_sp_ban SET so_luong_ban='$soLuong' WHERE id_sp_ban='$id_sp_ban' ";
                        $sqlQueryUpSpb=mysqli_query($dbConnect ,$sqlUpSpb);
                    }
                    else{
                        $sqlInsSpb="INSERT INTO tbl_sp_ban(id_sp,so_luong_ban) VALUES('$id_spb','$soLuong')";
                        $sqlQueryInsSpb=mysqli_query($dbConnect ,$sqlInsSpb);
                    }

                    /*Cập nhật lại số lượng sản phẩm */
                    $sqlCheckIdSp="SELECT * FROM tbl_sanpham WHERE id_sp='$id_spb'";
                    $sqlQueryCheckIdSp=mysqli_query($dbConnect ,$sqlCheckIdSp);
                    $rowSp=mysqli_fetch_array($sqlQueryCheckIdSp);
                    $so_luong_sp=$rowSp['so_luong']-$soLuongBan;
                    $sqlUpSp="UPDATE tbl_sanpham SET so_luong='$so_luong_sp' WHERE id_sp='$id_spb'";
                    $sqlQueryUpSp=mysqli_query($dbConnect ,$sqlUpSp);
            }

        }
        header("Location:quantri.php?page_layout=ct_dondathang&&id=".$id_hd);
    }
?>
<div><h2>Chi tiết đơn đặt hàng</h2></div>
     
    <div class="custumer-info">
        <h4>Thông tin khách hàng</h4>
        <ul>
            <li class="required">Tên <span class="hi"><?php echo $row['ten_kh']; ?></span></li>
            <li class="required">Mail <span class="hi"><?php echo $row['mail'] ?></span></li>
            <li class="required">Số điện thoại <span class="hi"><?php echo $row['sdt']; ?></span></li>
            <li class="required">Địa chỉ nhận hàng <span class="hi"><?php echo $row['noi_nhan']; ?></span></li>
        </ul>  
    </div>
    <div class="xuly">
        <div id='vien'>
            <form name="frmvien" action="" method="post">
                <ul>
                    <li >Người giao hàng <select name="giao_hang">
                                        <option value=''>Chọn nhân viên</option>
                                        <?php while($rowNv=mysqli_fetch_array($queryNv)) { ?>
                                            <option <?php if($rowNv['id_nvgh']==$row['id_nvgh']) echo "selected='selected'"; ?>
                                                value="<?php echo $rowNv['id_nvgh']; ?>"><?php echo $rowNv['ten_nvgh']; ?>          
                                            </option>
                                        <?php } ?>
                                    </select></li>
                    <li >Tình trạng <select name="tinh_trang">
                                        <?php while($rowsTt=mysqli_fetch_array($queryTt)) { ?>
                                            <option <?php if($rowsTt['id_tinh_trang']==$row['id_tinh_trang']) echo "selected='selected'"; ?>
                                                          value="<?php echo $rowsTt['id_tinh_trang']; ?>" ><?php echo $rowsTt['tinh_trang']; ?>
                                            </option>
                                        <?php }
                                        ?>
                                    </select>
                    </li>
                        Ghi chú</br><textarea value="" name="ghichu" onblur="noteBlur();" onfocus="noteFocus();"><?php if($row['ghi_chu'] !='') echo $row['ghi_chu']; else echo "Nhập ghi chú....";?></textarea>
                    <input type="submit" name="save" id="save" value="Lưu">
                 </ul>
            </form>
        </div>
        <div id='error'><?php if(isset($error)) echo $error; ?></div>
        <div id="submit-huy"><a onclick="return huyDdh();" href="quantri.php?page_layout=huyddh&&id=<?php echo $id_hd; ?>">Hủy đơn đặt hàng</a></div>
    </div>

    <h4>Thông tin sản phẩm</h4>
<table class="tbl-gio-hang" cellpadding="0" cellspacing="0">
    <tr id="title-gio-hang">
        <td width="15%">hình ảnh</td>
        <td width="41%">sản phẩm</td>
        <td width="12%">số lượng</td>
        <td width="16%">đơn giá</td>
        <td width="16%">thành tiền</td>  
    </tr>
    <?php
     $totalPriceAll=0;   
     while($rows=mysqli_fetch_array($querySp)){ ?>
    <tr class="iteam-gio-hang">
        <td width="15%"><img width="50" height="70" src="../anh/<?php echo $rows['anh_sp']; ?>" /></td>
        <td width="41%"><?php echo $rows['ten_sp']; ?></td>
        <td width="12%"><?php echo $rows['so_luong_mua'] ?></td>
        <td width="16%"><?php echo number_format($rows['don_gia']);?></td>
        <td width="16%"><?php echo number_format($totalPrice=$rows['so_luong_mua']*$rows['don_gia']); ?></td> 
    </tr>
    <?php 
        $totalPriceAll +=$totalPrice;
        } ?>
</table>
<table>
    <tr>
       <td id="total-price">Tổng giá trị: <span style="color:red"><?php echo number_format($totalPriceAll); ?></span> VNĐ</td>
    </tr>
</table>
