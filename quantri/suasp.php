<script>
$(function() {
    $( "#batdau_km" ).datepicker();
    $( "#ketthuc_km" ).datepicker();
});
</script>
<script type="text/javascript">
	function sub_addSp(){
		if(document.frmAddSp.ten_sp.value==""){
			alert('Tên sản phẩm không được để trống!');
			document.frmAddSp.ten_sp.focus();
			return false;
		}
		if(document.frmAddSp.gia_sp.value==""){
			alert('Giá sản phẩm không được để trống!');
			document.frmAddSp.gia_sp.focus();
			return false;
		}
		if(document.frmAddSp.so_luong.value==""){
			alert('Số lượng sản phẩm không được để trống!');
			document.frmAddSp.so_luong.focus();
			return false;
		}
		return true;
	}
</script>
<?php
	$id_sp=$_GET['id_sp'];
	$sql="SELECT * FROM tbl_sanpham WHERE id_sp=$id_sp";
	$query=mysqli_query($dbConnect ,$sql);
	$row=mysqli_fetch_array($query);
	
	$sqlDm="SELECT * FROM tbl_dm_sanpham";
	$queryDm=mysqli_query($dbConnect ,$sqlDm);
	
	if(isset($_POST['submit'])){

		$ten_sp=$_POST['ten_sp'];
		
		if($_FILES['anh_sp']['name']=='')
			$anh_sp=$_POST['anh_sp'];
		else{
			$anh_sp=$_FILES['anh_sp']['name'];
			$tmp=$_FILES['anh_sp']['tmp_name'];
		}
	
		
		$gia_sp=$_POST['gia_sp'];
		$so_luong=$_POST['so_luong'];
		$id_dm=$_POST['id_dm'];
		$kich_thuoc=$_POST['kich_thuoc'];
		$trong_luong=$_POST['trong_luong'];
		$mau_sac=$_POST['mau_sac'];
		$am_thanh=$_POST['am_thanh'];
		$bo_nho=$_POST['bo_nho'];
		$he_dieu_hanh=$_POST['he_dieu_hanh'];
		$the_nho=$_POST['the_nho'];
		$camera=$_POST['camera'];
		$pin=$_POST['pin'];
		$bao_hanh=$_POST['bao_hanh'];
		$ket_noi=$_POST['ket_noi'];
		$gia_km=$_POST['gia_km'];
		$batdau_km=$_POST['batdau_km'];
		$ketthuc_km=$_POST['ketthuc_km'];

	if(isset($ten_sp)&&isset($gia_sp)&&isset($so_luong)){
		if($_FILES['anh_sp']['name']!='')
		$upload=move_uploaded_file($tmp,'../anh/'.$anh_sp);
		
		$sqlUd="UPDATE tbl_sanpham 
					SET ten_sp='$ten_sp', anh_sp='$anh_sp', gia_sp='$gia_sp', so_luong='$so_luong', id_dm='$id_dm',
						kich_thuoc='$kich_thuoc',trong_luong='$trong_luong',mau_sac='$mau_sac',am_thanh='$am_thanh',
						bo_nho='$bo_nho',he_dieu_hanh='$he_dieu_hanh',the_nho='$the_nho',camera='$camera',pin='$pin',
						bao_hanh='$bao_hanh',ket_noi='$ket_noi',gia_km='$gia_km',batdau_km='$batdau_km',ketthuc_km='$ketthuc_km'
					WHERE id_sp=$id_sp";
		$queryUd=mysqli_query($dbConnect ,$sqlUd);
		header('location:quantri.php?page_layout=danhsachsp');
		}
	}
	
	?>
<div id="body">
	<h2>sửa thông tin sản phẩm</h2>
    <form method="post" name="frmAddSp" enctype="multipart/form-data">
    <div class="main">
    	<div class="line"><label>Tên sản phẩm<span style="color:red">*</span></label> <input type="text" name="ten_sp" value="<?php if(isset($ten_sp)) echo $ten_sp; else echo $row['ten_sp']; ?>"/></div>
        <div class="line"><label>Hình ảnh<span style="color:red">*</span></label> <input type="file" name="anh_sp" /><input type="hidden" name="anh_sp" value="<?php echo $row['anh_sp']; ?>"/></div>
        <div class="line"><label>Nhà cung cấp<span style="color:red">*</span></label>  <select name="id_dm">
        														<?php while($rowDm=mysqli_fetch_array($queryDm)){ ?>
                                                                <option <?php if($rowDm['id_dm']==$row['id_dm']) echo "selected='selected'"; ?> 
                                                                		value="<?php echo $rowDm['id_dm']; ?>"> <?php echo $rowDm['ten_dm']; ?>
                                                                </option>
                                                                <?php } ?>
                                                       </select></div>
        <div class="line"><label>Giá<span style="color:red">*</span></label> <input type="text" name="gia_sp" value="<?php if(isset($gia_sp)) echo $gia_sp; else echo $row['gia_sp']; ?>"/></div>
        <div class="line"><label>Số lượng<span style="color:red">*</span></label> <input type="text" name="so_luong" value="<?php if(isset($so_luong)) echo $so_luong; else echo $row['so_luong']; ?>"/></div>
    	<div class="line"><label>Kích thước</label> <input type="text" name="kich_thuoc" value="<?php echo $row['kich_thuoc']; ?>"/></div>
        <div class="line"><label>Trọng lượng</label> <input type="text" name="trong_luong" value="<?php echo $row['trong_luong'];?>"/></div>
        <div class="line"><label>Màu sắc</label> <input type="text" name="mau_sac" value="<?php echo $row['mau_sac']; ?>"/></div>
        <div class="line"><label>Âm thanh</label> <input type="text" name="am_thanh" value="<?php echo $row['am_thanh']; ?>"/></div>
    </div>
    <div class="main">
        <div class="line"><label>Bộ nhớ</label> <input type="text" name="bo_nho" value="<?php echo $row['bo_nho']; ?>"/></div>
        <div class="line"><label>Hệ điều hành</label> <input type="text" name="he_dieu_hanh" value="<?php echo $row['he_dieu_hanh']; ?>"/></div>
        <div class="line"><label>Thẻ nhớ</label> <input type="text" name="the_nho" value="<?php echo $row['the_nho'];?>"/></div>
        <div class="line"><label>Camera</label> <input type="text" name="camera" value="<?php echo $row['camera']; ?>"/></div>
        <div class="line"><label>Pin</label> <input type="text" name="pin" value="<?php echo $row['pin']; ?>"/></div>
        <div class="line"><label>Bảo hành</label> <input type="text" name="bao_hanh" value="<?php echo $row['bao_hanh']; ?>"/></div>
        <div class="line"><label>Kết nối</label> <input type="text" name="ket_noi" value="<?php echo $row['ket_noi']; ?>"/></div>  
        <div class="line"><label>Giá K/m</label> <input type="text" name="gia_km" value="<?php echo $row['gia_km']; ?>"/></div>  
        <div id="tg_km">Khuyến mãi từ: <input type="text" name="batdau_km" id="batdau_km" value="<?php echo $row['batdau_km']; ?>" >
        						   Đến <input type="text" name="ketthuc_km" id="ketthuc_km" value="<?php echo $row['ketthuc_km']; ?>" ></div>
    </div>
    <div id="submit"><input type="submit" name="submit" value="Cập nhật" onclick="return sub_addSp()";/> <input type="reset" name="reset" value="Làm mới" /></div>
    </form>
</div>