<script>
$(function() {
    $( "#batdau_km" ).datepicker();
    $( "#ketthuc_km" ).datepicker();
});
</script>
<script type="text/javascript">
	function sub_addSp(){
		if(document.frmAddSp.ten_sp.value==""){
			alert('Tên sản phẩm không được để trống!');
			document.frmAddSp.ten_sp.focus();
			return false;
		}
		if(document.frmAddSp.gia_sp.value==""){
			alert('Giá sản phẩm không được để trống!');
			document.frmAddSp.gia_sp.focus();
			return false;
		}
		if(document.frmAddSp.so_luong.value==""){
			alert('Số lượng sản phẩm không được để trống!');
			document.frmAddSp.so_luong.focus();
			return false;
		}
		return true;
	}
</script>
<?php
	$sqlDm="SELECT * FROM tbl_dm_sanpham";
	$queryDm=mysqli_query($dbConnect ,$sqlDm);

	if(isset($_POST['submit'])){

		$ten_sp=$_POST['ten_sp'];
		
		if($_FILES['anh_sp']['name']=='')
			$anh_sp=$_POST['anh_sp'];
		else{
			$anh_sp=$_FILES['anh_sp']['name'];
			$tmp=$_FILES['anh_sp']['tmp_name'];
		}

		$gia_sp=$_POST['gia_sp'];
		$so_luong=$_POST['so_luong'];	
		$id_dm=$_POST['id_dm'];
		$kich_thuoc=$_POST['kich_thuoc'];
		$trong_luong=$_POST['trong_luong'];
		$mau_sac=$_POST['mau_sac'];
		$am_thanh=$_POST['am_thanh'];
		$bo_nho=$_POST['bo_nho'];
		$he_dieu_hanh=$_POST['he_dieu_hanh'];
		$the_nho=$_POST['the_nho'];
		$camera=$_POST['camera'];
		$pin=$_POST['pin'];
		$bao_hanh=$_POST['bao_hanh'];
		$ket_noi=$_POST['ket_noi'];
		
	if(isset($ten_sp)&&isset($gia_sp)&&isset($so_luong)){
		if($_FILES['anh_sp']['name']!='')
		$upload=move_uploaded_file($tmp,'../anh/'.$anh_sp);
		$sqlIn="INSERT INTO 
						tbl_sanpham(ten_sp,anh_sp,id_dm,gia_sp,so_luong,kich_thuoc,trong_luong,mau_sac,am_thanh,bo_nho,he_dieu_hanh,the_nho,camera,pin,bao_hanh,ket_noi) 
				VALUES('$ten_sp','$anh_sp','$id_dm','$gia_sp','$so_luong','$kich_thuoc','$trong_luong','$mau_sac','$am_thanh','$bo_nho','$he_dieu_hanh','$the_nho','$camera','$pin','$bao_hanh','$ket_noi')";
		$queryIn=mysqli_query($dbConnect ,$sqlIn);
		header('location:quantri.php?page_layout=danhsachsp');
		}
	}
	
	?>
<div id="body">
	<h2>thêm sản phẩm mới</h2>
    <form method="post" name="frmAddSp" enctype="multipart/form-data">
    <div class="main">
    	<div class="line"><label>Tên sản phẩm<span style="color:red">*</span></label><input type="text" name="ten_sp" value=""/></div>
        <div class="line"><label>Hình ảnh<span style="color:red">*</span></label><input type="file" name="anh_sp" /></div>
        <div class="line"><label>Nhà sản xuất<span style="color:red">*</span></label>  <select name="id_dm">
        														<?php while($rowDm=mysqli_fetch_array($queryDm)){ ?>
                                                                <option 
                                                                		value="<?php echo $rowDm['id_dm']; ?>"><?php echo $rowDm['ten_dm']; ?>
                                                                </option>
                                                                <?php } ?>
                                                       </select></div>
        <div class="line"><label>Giá<span style="color:red">*</span></label><input type="text" name="gia_sp" value=""/></div>
        <div class="line"><label>Số lượng<span style="color:red">*</span></label> <input type="text" name="so_luong" value=""/></div>
        <div class="line"><label>Kích thước</label> <input type="text" name="kich_thuoc" value=""/></div>
        <div class="line"><label>Trọng lượng</label> <input type="text" name="trong_luong" value=""/></div>
        <div class="line"><label>Màu sắc</label> <input type="text" name="mau_sac" value=""/></div>
        <div class="line"><label>Âm thanh</label> <input type="text" name="am_thanh" value=""/></div>
    </div>
    <div class="main">
    	<div class="line"><label>Bộ nhớ</label> <input type="text" name="bo_nho" value=""/></div>
        <div class="line"><label>Hệ điều hành</label> <input type="text" name="he_dieu_hanh" value=""/></div>
        <div class="line"><label>Thẻ nhớ</label> <input type="text" name="the_nho" value=""/></div>
        <div class="line"><label>Camera</label> <input type="text" name="camera" value=""/></div>
        <div class="line"><label>Pin</label> <input type="text" name="pin" value=""/></div>
        <div class="line"><label>Bảo hành</label> <input type="text" name="bao_hanh" value=""/></div>
        <div class="line"><label>Kết nối</label> <input type="text" name="ket_noi" value=""/></div>  
        <div class="line"><label>Giá K/m</label> <input type="text" name="gia_km" value=""/></div>  
        <div id="tg_km">Khuyến mãi từ: <input type="text" name="batdau_km" id="batdau_km"> Đến <input type="text" name="ketthuc_km" id="ketthuc_km"></div>
    </div>
     	<div id="submit"><input type="submit" name="submit" value="Cập nhật" onclick="return sub_addSp()"; /> <input type="reset" name="reset" value="Làm mới" /></div>
    </form>
</div>