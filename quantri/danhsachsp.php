<script language='javascript'>
	function xoaSanpham(){
		var conf=confirm('Bạn có chắc chắn muốn xóa sản phẩm này không?');
		return conf;
	}
	function thongBao(){
		if(xoaSanpham()==1)
		alert('Bạn đã xóa thành công sản phẩm');
	}
</script>
<?php
	if(isset($_GET['page']))
		$page=$_GET['page'];
	else
		$page=1;
	$rowPerPage=5;
	$firstRow=$rowPerPage*($page-1);

	$sql="SELECT * FROM tbl_sanpham INNER JOIN tbl_dm_sanpham ON tbl_sanpham.id_dm=tbl_dm_sanpham.id_dm ORDER BY id_sp DESC LIMIT $firstRow, $rowPerPage";
	$query=mysqli_query($dbConnect ,$sql);
	
	$totalRows=mysqli_num_rows(mysqli_query($dbConnect ,"SELECT * FROM tbl_sanpham"));
	$totalPage=ceil($totalRows/$rowPerPage);
	
	$listPage='';
	for($i=1;$i<=$totalPage;$i++){
		if($i!=$page)
			$listPage.="<a href=\"quantri.php?page_layout=danhsachsp&&page=$i\">".$i." </a>";
		else
			$listPage.="<span style=\"color:red\">$i</span> ";
	}
?>
<div id="body">
    <h2>sản phẩm</h2>
    <div id="main">
        <table id="prds" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr id="prd-bar">
                <td width="4%">ID</td>
                <td width="28%">Tên sản phẩm</td>
                <td width="20%">Nhà cung cấp</td>
                <td width="13%">Ảnh mô tả</td>
                <td width="15%">Giá</td>
                <td width="10%">Số lượng</td>
                <td width="5%">Sửa</td>
                <td width="5%">Xóa</td>
            </tr>
            <?php while($row=mysqli_fetch_array($query)){ ?>
            <tr>
                <td><span><?php echo $row['id_sp']; ?></span></td>
                <td class="l5"><a href="../index.php?page_layout=chitietsp&&id_sp=<?php echo $row['id_sp']; ?>"><?php echo $row['ten_sp']; ?></a></td>
                <td class="l5"><?php echo $row['ten_dm']; ?></td>
                <td><span class="thumb"><img width="60" src="../anh/<?php echo $row['anh_sp']; ?>" /></span></td>
                <td class="l5"><span class="price"><?php echo number_format($row['gia_sp']); ?></span></td>
                <td class="l5"><?php echo $row['so_luong']; ?></td>
                <td><a href="quantri.php?page_layout=suasp&&id_sp=<?php echo $row['id_sp'] ?>"><span>Sửa</span></a></td>
                <td><a onclick="return xoaSanpham()"; href="xoasp.php?id_sp=<?php echo $row['id_sp']; ?>"><span>Xóa</span></a></td>
            </tr> 
            <?php } ?>
        </table>
        <p id="pagination"><?php echo $listPage ?></p>
    </div>
</div>