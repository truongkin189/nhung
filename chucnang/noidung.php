<?php
    include_once('function.php');
	$sql="SELECT * FROM tbl_sanpham ORDER BY id_sp DESC LIMIT 6";
	$query=mysqli_query($dbConnect ,$sql);
	$i=1;
    $sqlSpBc="SELECT * FROM tbl_sp_ban INNER JOIN tbl_sanpham WHERE tbl_sanpham.id_sp=tbl_sp_ban.id_sp ORDER BY so_luong_ban DESC LIMIT 6";
    $querySpBc=mysqli_query($dbConnect ,$sqlSpBc);
?>
<div class="prd-block">
    <h2>điện thoại mới</h2>
    <div class="prd-list">
    	<?php while($rows=mysqli_fetch_array($query)) { ?>
        <?php
            if(isset($rows['batdau_km']))
                $batdau_km=strtotime($rows['batdau_km']);
            else
                $batdau_km=0;
            if(isset($rows['ketthuc_km']))
                $ketthuc_km=strtotime($rows['ketthuc_km']);
            else
                $ketthuc_km=0;
            $date_now=strtotime(gmdate("Y-m-d", time()+7*3600));
        ?>
        <div class="prd-item">
            <a href="index.php?page_layout=chitietsp&&id_sp=<?php echo $rows['id_sp']; ?>"><img width="80" height="144" src="anh/<?php echo $rows['anh_sp']; ?>"></a>
            <h3><a href="index.php?page_layout=chitietsp&&id_sp=<?php echo $rows['id_sp']; ?>"><?php echo $rows['ten_sp']; ?></a></h3>
            <p>Tình trạng: <?php if($rows['so_luong']>0) echo "còn hàng"; else echo "hết hàng"; ?></p>
            <p class="km"><?php if(checkTime($batdau_km,$ketthuc_km,$date_now)) echo number_format($rows['gia_sp'],0,",",".").' VNĐ'; ?></p>
            <p class="price"><span>Giá: <?php if(checkTime($batdau_km,$ketthuc_km,$date_now)) echo number_format($rows['gia_km'],0,",",".");
                                               else echo number_format($rows['gia_sp'],0,",","."); ?> VNĐ</span></p>
        </div>
        <?php 
			if($i%3==0)
         		echo '<div class="clear"></div>';
				$i++;
		}
		?>
    </div>
</div>  

<div class="prd-block">
    <h2>điện thoại bán chạy</h2>
    <div class="prd-list">
            <?php while($rows=mysqli_fetch_array($querySpBc)) { 
            if(isset($rows['batdau_km']))
                $batdau_km=strtotime($rows['batdau_km']);
            else
                $batdau_km=0;
            if(isset($rows['ketthuc_km']))
                $ketthuc_km=strtotime($rows['ketthuc_km']);
            else
                $ketthuc_km=0;
            $date_now=strtotime(gmdate("Y-m-d", time()+7*3600));
        ?>
        <div class="prd-item">
            <a href="index.php?page_layout=chitietsp&&id_sp=<?php echo $rows['id_sp']; ?>"><img width="80" height="144" src="anh/<?php echo $rows['anh_sp']; ?>"></a>
            <h3><a href="index.php?page_layout=chitietsp&&id_sp=<?php echo $rows['id_sp']; ?>"><?php echo $rows['ten_sp']; ?></a></h3>
            <p>Tình trạng: <?php if($rows['so_luong']>0) echo "còn hàng"; else echo "hết hàng"; ?></p>
            <p class="km"><?php if(checkTime($batdau_km,$ketthuc_km,$date_now)) echo number_format($rows['gia_sp'],0,",",".").' VNĐ'; ?></p>
            <p class="price"><span>Giá: <?php if(checkTime($batdau_km,$ketthuc_km,$date_now)) echo number_format($rows['gia_km'],0,",",".");
                                               else echo number_format($rows['gia_sp'],0,",","."); ?> VNĐ</span></p>
        </div>
        <?php 
        if($i%3==0)
                echo '<div class="clear"></div>';
                $i++;
        }
        ?>
    </div>
</div>                             