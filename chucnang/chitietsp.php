<script type="text/javascript" src="javascrip/library.js"></script>
<?php 
    include_once('function.php');
	$id=$_GET['id_sp'];
	$sql="SELECT * FROM tbl_sanpham INNER JOIN tbl_dm_sanpham ON tbl_sanpham.id_dm=tbl_dm_sanpham.id_dm WHERE tbl_sanpham.id_sp=$id";
	$query=mysqli_query($dbConnect ,$sql);
	$row=mysqli_fetch_array($query);

    $sqlBl="SELECT * FROM tbl_sanpham INNER JOIN tbl_binhluan ON tbl_sanpham.id_sp=tbl_binhluan.id_sp WHERE tbl_sanpham.id_sp=$id";
    $queryBl=mysqli_query($dbConnect ,$sqlBl);

    if(isset($_POST['submit'])){
        $ho_ten=$_POST['ho_ten'];
        $dien_thoai=$_POST['dien_thoai'];
        $noi_dung=$_POST['noi_dung'];
        $ngay_gio=date("Y-m-d G:i:s");

        $sqlInsComment="INSERT INTO tbl_binhluan(id_sp,ho_ten,ngay_gio,noi_dung) VALUES('$id','$ho_ten','$ngay_gio','$noi_dung')";
        $queryInsComment=mysqli_query($dbConnect ,$sqlInsComment);
        header('location:');
    }
?>
<?php
    if(isset($row['batdau_km']))
        $batdau_km=strtotime($row['batdau_km']);
    else
        $batdau_km=0;
    if(isset($row['ketthuc_km']))
        $ketthuc_km=strtotime($row['ketthuc_km']);
    else
        $ketthuc_km=0;
    $date_now=strtotime(gmdate("Y-m-d", time()+7*3600));
?>
<div id="detail-product">
    <h2 id="title-pro">thông tin chi tiết</h2>
    
    <div id="pro-detail">
        <div id="img-pro"><img width="170" height="250" src="anh/<?php echo $row['anh_sp']; ?>" /> </div>
        <div id="detail">
            <ul>
                <li id="name-detail"><h2><?php echo $row['ten_sp']; ?> </h2></li>
                <li id="fac-detail"><span>Hãng sản xuất:</span> <?php echo $row['ten_dm']; ?></li>
                <li id="price-detail"><span>Giá:</span> <?php if(checkTime($batdau_km,$ketthuc_km,$date_now)) echo number_format($row['gia_km'],0,",",".");
                                               else echo number_format($row['gia_sp'],0,",","."); ?> VNĐ</li>
                <?php if(checkTime($batdau_km,$ketthuc_km,$date_now))
                     echo '<li id="gia-cu"> Giá cũ: '.number_format($row['gia_sp'],0,",",".").' VNĐ </li>'; ?>
                <li id="status-detail"><span>Tình trạng:</span> Còn <?php echo $row['so_luong']; ?> sản phẩm</li>
                <div id="mua-hang"><a href="chucnang/themhang.php?id_sp=<?php echo $row['id_sp']; ?>">Đặt hàng</a></div>
            </ul>
        </div>
    </div>
 </div> 

<div class="tabbed">
    <ul class="tabnav">
        <li><a href="#tab1">Chi tiết sản phẩm</a></li>
        <li><a href="#tab2">Bình luận sản phẩm</a></li>
    </ul>
    <div class="tabcont"> 
            <div id="tab1" class="tabcontent">
                
                <table id="descrip-prd" cellpadding="0" cellspacing="0">
                    <tr class="info-detail">
                        <td class="title-info-1">Kích thước</td>
                        <td class="info-detail-2"><?php echo $row['kich_thuoc']; ?></td>
                    </tr>

                    <tr class="info-detail">
                        <td class="title-info-1">Trọng lượng</td>
                        <td class="info-detail-2"><?php echo $row['trong_luong']; ?></td>
                    </tr>

                     <tr class="info-detail">
                        <td class="title-info-1">Màu sắc</td>
                        <td class="info-detail-2"><?php echo $row['mau_sac']; ?></td>
                    </tr>

                    <tr class="info-detail">
                        <td class="title-info-1">Âm thanh</td>
                        <td class="info-detail-2"><?php echo $row['am_thanh']; ?></td>
                    </tr>

                    <tr class="info-detail">
                        <td class="title-info-1">Bộ nhớ</td>
                        <td class="info-detail-2"><?php echo $row['bo_nho']; ?></td>
                    </tr>

                     <tr class="info-detail">
                        <td class="title-info-1">Hệ điều hành</td>
                        <td class="info-detail-2"><?php echo $row['he_dieu_hanh']; ?></td>
                    </tr>

                    <tr class="info-detail">
                        <td class="title-info-1">Thẻ nhớ</td>
                        <td class="info-detail-2"><?php echo $row['the_nho']; ?></td>
                    </tr>
                    
                    <tr class="info-detail">
                        <td class="title-info-1">Camera</td>
                        <td class="info-detail-2"><?php echo $row['camera']; ?></td>
                    </tr>
                    
                    <tr class="info-detail">
                        <td class="title-info-1">Pin</td>
                        <td class="info-detail-2"><?php echo $row['pin']; ?></td>
                    </tr>
                    
                    <tr class="info-detail">
                        <td class="title-info-1">Bảo hành</td>
                        <td class="info-detail-2"><?php echo $row['bao_hanh']; ?></td>
                    </tr>

                    <tr class="info-detail">
                        <td class="title-info-1">Kết nối</td>
                        <td class="info-detail-2"><?php echo $row['ket_noi']; ?></td>
                    </tr>
                </table>
            </div>



            <!--Comment-->
            <div id="tab2" class="tabcontent">
                
                <!--Comment List-->
                <div class="comment-list">
                    <ul>
                        <li>Sản phẩm tuyệt vời</li>
                        <li><span style="font-weight:bold">Thanh</span> | 2014-3-2 09:16:13</li>
                    </ul>
                    <?php while($rows=mysqli_fetch_array($queryBl)) { ?>
                    <ul>
                        <li><?php echo $rows['noi_dung']; ?></li>
                        <li><span style="font-weight:bold"><?php echo $rows['ho_ten']; ?></span> | <?php echo $rows['ngay_gio']; ?></li>
                    </ul>
                    <?php } ?>
                </div>
            
            <!--End comment list-->

                <div class="prd-comment">
                    <h3>Bình luận sản phẩm</h3>
                    <form action="" method="post">
                        <ul>
                            <li class="required">Tên <br><input type="text" name="ho_ten"> <span></span></li>
                            <li class="required">Số điện thoại <br><input type="text" name="dien_thoai"><span></span></li>
                            <li class="required">Nội dung <br><textarea name="noi_dung"></textarea> <span></span></li>
                            <li><input type="submit" value="Bình luận" name="submit"></li>
                        </ul>
                    </form>
               
                </div>
            <!--End comment-->
             </div>
    </div>
</div>

