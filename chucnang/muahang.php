<script type="text/javascript" src="javascrip/library.js"></script>
<script type="text/javascript">
    function sub_muahang() {
        if (document.frmMuaHang.ten_kh.value=="") {
            alert('Bạn chưa nhập Họ tên');
            document.frmMuaHang.ten_kh.focus();
            return false;
        }
        if (document.frmMuaHang.mail.value=="") {
            alert('Bạn chưa nhập Mail');
            document.frmMuaHang.mail.focus();
            return false;
        }
        if (document.frmMuaHang.dien_thoai.value=="") {
            alert('Bạn chưa nhập Điện thoại');
            document.frmMuaHang.dien_thoai.focus();
            return false;
        }
        if (document.frmMuaHang.dc_nhan.value=="") {
            alert('Bạn chưa nhập Địa chỉ nhận');
            document.frmMuaHang.dc_nhan.focus();
            return false;
        }
        if (!checkMail(document.frmMuaHang.mail.value)) {
            alert('Mail bạn nhập không đúng định dạng');
            document.frmMuaHang.mail.focus();
            return false;
        }
        return true;
    }
</script>
<?php 
include_once('function.php');
    ob_start();
    $arrayId = array();
    foreach ($_SESSION['giohang'] as $id_sp => $so_luong) 
        $arrayId[]=$id_sp;

        $strId=implode(",",$arrayId); //Tạo chuỗi $strId từ mảng $arrayId
        $sql="SELECT * FROM tbl_sanpham WHERE id_sp IN($strId)";
        $query=mysqli_query($dbConnect ,$sql);


        $sqlSelectId="SELECT max(id_kh) as Max FROM tbl_khachhang";
        $querySelectId=mysqli_query($dbConnect ,$sqlSelectId);
        $rowId=mysqli_fetch_array($querySelectId);
        $id_kh=$rowId['Max']+1;
        //$ngay_gio=date("Y-m-d G:i:s");
        $ngay_gio=gmdate("Y-m-d G:i:s", time()+7*3600);

        $totalPriceAll=0;
?>

<div id="gio-hang"><h2>xác nhận hóa đơn</h2></div>
     <h4>Thông tin khách hàng</h4>
    <div class="custumer-info">
    <form action="" name="frmMuaHang" method="post">
        <ul>
            <li class="required">Tên <input type="text" name="ten_kh"/><span style="color:red">*</span></li>
            <li class="required">Mail <input type="text" name="mail"><span style="color:red">*</span></li>
            <li class="required">Số điện thoại <input type="text" name="dien_thoai"><span style="color:red">*</span></li>
            <li class="required">Địa chỉ nhận hàng <input type="text" name="dc_nhan"><span style="color:red">*</span></li>
        </ul>  
    </div>

    <h4>Thông tin sản phẩm</h4>
<table class="tbl-gio-hang" cellpadding="0" cellspacing="0" id="muahang">
    <tr id="title-gio-hang">
        <td width="15%">hình ảnh</td>
        <td width="41%">sản phẩm</td>
        <td width="12%">số lượng</td>
        <td width="16%">đơn giá</td>
        <td width="16%">thành tiền</td>  
    </tr>


    <?php while ($rows=mysqli_fetch_array($query)) { ?>
    <?php
         if(isset($rows['batdau_km']))
            $batdau_km=strtotime($rows['batdau_km']);
         else
            $batdau_km=0;
         if(isset($rows['ketthuc_km']))
            $ketthuc_km=strtotime($rows['ketthuc_km']);
         else
            $ketthuc_km=0;
         $date_now=strtotime(gmdate("Y-m-d", time()+7*3600));
         if(checkTime($batdau_km,$ketthuc_km,$date_now)) $gia=$rows['gia_km']; else  $gia=$rows['gia_sp'];

         $id_sp=$rows['id_sp'];
         $so_luong_mua=$_SESSION['giohang'][$id_sp];
         $totalPrice=$so_luong_mua*$gia;
        // $price=$rows['gia_sp'];

         $chi_tiet[$id_sp][$id_sp]=$id_sp;
         $chi_tiet[$id_sp]['so_luong_mua']=$so_luong_mua;
         $chi_tiet[$id_sp]['totalPrice']=$gia;
        // echo $chi_tiet[$id_sp][$id_sp];
     ?>
    <tr class="iteam-gio-hang">
        <td width="15%"><img width="50" height="70" src="anh/<?php echo $rows['anh_sp']; ?>" /></td>
        <td width="41%"><?php echo $rows['ten_sp']; ?></td>
        <td width="12%"><?php echo $so_luong_mua; ?></td>
        <td width="16%"><?php echo number_format($gia); ?></td>
        <td width="16%"><?php echo number_format($totalPrice); ?></td> 
    </tr>
    <?php 
        $totalPriceAll += $totalPrice;
        } 
        /*foreach ($chi_tiet as $key => $value) {
            echo "</br>";
            $arrayValue = array();
            foreach ($chi_tiet[$key] as $world) {
               // echo $hello."=>".$world;
                $arrayValue[]=$world;
            }
            echo $arrayValue[0]." ".$arrayValue[1]." ".$arrayValue[2];
        }*/
    ?>
</table>
<table>
    <tr><td colspan="5" id="total-price">Tổng giá trị: <span style="color:red"><?php echo number_format($totalPriceAll); ?></span> VNĐ</td></tr>
</table>

<div id="submit"><input type="submit" value="Gửi phiếu đặt hàng" name="submit" onclick="return sub_muahang()";></div></form>
<?php
    if(isset($_POST['submit'])){
        $ten_kh=$_POST['ten_kh'];
        $dien_thoai=$_POST['dien_thoai'];
        $mail=$_POST['mail'];
        $dc_nhan=$_POST['dc_nhan'];

        //Insert thông tin khách hàng vào bảng tbl_khachhang
        if(isset($ten_kh)&&isset($dien_thoai)&&isset($mail)&&isset($dc_nhan)){
            $sqlInsCusm="INSERT INTO tbl_khachhang(ten_kh,sdt, mail) VALUES('$ten_kh','$dien_thoai','$mail')";
            $queryInCusm=mysqli_query($dbConnect ,$sqlInsCusm);
        }
        // Insert thông tin vào bảng đơn đặt hàng
        $sqlInsHd="INSERT INTO tbl_don_dh(id_kh,id_tinh_trang,ngay_lap,tong_gia,noi_nhan) 
                       VALUES('$id_kh','1','$ngay_gio','$totalPriceAll','$dc_nhan')";
        $queryInsHd=mysqli_query($dbConnect ,$sqlInsHd);

        /*Insert thông tin vào bảng chi tiết đơn đặt hàng
          Tìm max id_don_dh là Id vừa cập nhật vào bảng tbl_don_dh,
          Với mỗi mảng $chi_tiet[$id_sp] sẽ lấy thông tin cập nhật
          vào bảng tbl_ct_ddh   */
        $sqlSelectIdHd="SELECT max(id_hd) as Max FROM tbl_don_dh";
        $querySelectIdHd=mysqli_query($dbConnect ,$sqlSelectIdHd);
        $rowIdHd=mysqli_fetch_array($querySelectIdHd);
        $id_hd_max=$rowIdHd['Max'];
        foreach ($chi_tiet as $id_sp => $array) {
            
            $arrayValue= array();
            foreach ($chi_tiet[$id_sp] as $value) {
                $arrayValue[]=$value;
            }
            $sqlInsCtHd="INSERT INTO tbl_ct_ddh(id_hd,id_sp,so_luong_mua,don_gia) 
                                VALUES('$id_hd_max','$arrayValue[0]','$arrayValue[1]','$arrayValue[2]')";
            $sqlQueryCtHd=mysqli_query($dbConnect ,$sqlInsCtHd);

        }
    header('location:index.php?page_layout=hoanthanh');

    }

?>